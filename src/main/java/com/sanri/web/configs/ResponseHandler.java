package com.sanri.web.configs;

import com.sanri.web.dto.ResponseDto;

public interface ResponseHandler<T> {
    /**
     * 处理输出结果
     * @param responseDto
     * @return
     */
    T handlerOut(ResponseDto responseDto);

    /**
     * 处理异常结果
     * @param responseDto
     * @return
     */
    T handlerError(ResponseDto responseDto);
}
