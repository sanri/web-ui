package com.sanri.web.configs;

@FunctionalInterface
public interface ParamHandler {
    String handler(String origin);
}
