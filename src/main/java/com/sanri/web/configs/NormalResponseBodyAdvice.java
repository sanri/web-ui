package com.sanri.web.configs;

import com.sanri.web.dto.ResponseDto;
import com.sanri.web.dto.TreeResponseDto;
import com.sanri.web.helper.TreeResponseDtoHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.core.ReflectUtils;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Executable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 正常数据收集处理
 */
@RestControllerAdvice
@Slf4j
@Order(Ordered.LOWEST_PRECEDENCE - 2)
public class NormalResponseBodyAdvice extends AbstractResponseBodyAdvice {


    public boolean supports(MethodParameter returnType, Class converterType) {
        boolean supports = super.supports(returnType, converterType);
        if(!supports)return supports;

        Executable executable = returnType.getExecutable();
        Class<?> declaringClass = executable.getDeclaringClass();
        RestControllerAdvice controllerAdvice = declaringClass.getAnnotation(RestControllerAdvice.class);
        ExceptionHandler exceptionHandler = executable.getAnnotation(ExceptionHandler.class);
        if(controllerAdvice != null && exceptionHandler != null){
            return false;
        }

        return true;
    }

    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        Executable executable = returnType.getExecutable();
        AnnotatedType annotatedReturnType = executable.getAnnotatedReturnType();
        Type type = annotatedReturnType.getType();

        // 如果空，则为成功
        if(type == Void.TYPE){
            // 空返回直接返回成功
            return  ResponseDto.ok();
        }
        //如果本身返回 ResponseDto 则不处理
        if(type == ResponseDto.class){
            return body;
        }
        //检查是否需要树状结构返回
        TreeResponse treeResponse = executable.getAnnotation(TreeResponse.class);
        if(treeResponse != null){
            //检查原始返回类型是否为 List 类型，否则给出异常
            if((type instanceof ParameterizedTypeImpl) && ((ParameterizedTypeImpl) type).getRawType() == List.class) {
                List<TreeResponseDto> modifyList = new ArrayList();
                Class<? extends TreeResponseDto> treeReposeDtoClass = treeResponse.type();
                String rootId = treeResponse.rootId();
                String rootParentId = treeResponse.rootParentId();

                // 转换结构成可支持树结构
                List results = (List) body;
                for (Object result : results) {
                    TreeResponseDto newInstance = (TreeResponseDto) ReflectUtils.newInstance(treeReposeDtoClass, new Class<?>[]{result.getClass()}, new Object[]{result});
                    modifyList.add(newInstance);
                }

                //转换成树结构，根据需要转成森林或树结构
                if (StringUtils.isNotBlank(rootId)) {
                    body = TreeResponseDtoHelper.fastConvertTree(modifyList, rootId);
                } else if (StringUtils.isNotBlank(rootParentId)) {
                    body = TreeResponseDtoHelper.fastConvertForest(modifyList, rootParentId);
                }

                return ResponseDto.ok().data(body);
            }
            throw new IllegalArgumentException("树类型返回要求原始数据为 List 类型");
        }

        return ResponseDto.ok().data(body);
    }
}
