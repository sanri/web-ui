package com.sanri.web.dto;

import lombok.*;

import java.util.List;

@Data
public class PageResponseDto<T> {
    private List<T> rows;
    private Integer total;

    public PageResponseDto() {
    }

    public PageResponseDto(List<T> rows) {
        this.rows = rows;
        if(rows != null){
            total = rows.size();
        }
    }
}
