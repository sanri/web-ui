package com.sanri.web.exception;


import com.sanri.web.dto.ResponseDto;

import java.text.MessageFormat;

public interface ExceptionCause<T extends Exception> {
    T exception(Object... args);

    ResponseDto result();

    MessageFormat getMessageFormat();
}
