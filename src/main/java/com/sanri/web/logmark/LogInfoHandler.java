package com.sanri.web.logmark;

public interface LogInfoHandler {
    void before(LogInfo logInfo);

    void around(AroundLogInfo aroundLogInfo);
}
